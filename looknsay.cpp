/*******************************************************************************
Program: LookNSay Numbers
Author:  Karan Boodwa
Created: October 4 2014
Usage:   programname n
		 where n is an int
Description: Calculates the nth "looknsay" number, where n is an int provided
             as a command line argument. A looknsay number describes the number
             given.
             e.g.: 21 translates to one '2' and one '1' so the next number is
                   1211 (one 2 one 1) etc. etc.
             This program starts with 1 as the default 1st looknsay number
*******************************************************************************/

#include <iostream>
#include <cctype>
#include <sstream>
#include <string>

using namespace std;

int main(int argc, char* argv[]) {

    cout << "LookNSay Numbers!" << endl;
    int n = 0;

    if( argc >= 2) {
        istringstream ss(argv[1]);
        if( !(ss >> n)) {
            cerr << "Invalid Number: " << argv[1] << endl;
            return -1;
        }
        //cout << n << endl;
    }
    else{
        cerr << "Usage: programname int" << endl;
    }

    string final = "1";
    string currString = "";

    cout << final << endl;

    for(int i = 0; i < n-1; i++)
    {
        istringstream ss(final);
        char prev = final[0];
        char currChar;
        int count = 0;

        for(int i = 0; i < final.length(); i++){
            currChar = final[i];
            if(currChar == prev){
                count++;
            }
            else{
                // Convert count to string
                ostringstream iToStr;
                iToStr << count;
                string s = iToStr.str();

                // Concat count and the previous char
                currString = currString + s + prev;

                // Keep track of newly seen char
                prev = currChar;
                count = 1;
            }
        }

        ostringstream iToStr;
        iToStr << count;
        string s = iToStr.str();
        currString = currString + s + currChar;
        final = currString;
        currString = "";
        cout << final << endl;

    }

    return 0;
}